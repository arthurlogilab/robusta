#!/usr/bin/env python3
# ------------------------------------------------------------------------------
#   This program is free software: you can redistribute it and/or modify it
#   under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#   License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

from setuptools import setup
from setuptools import find_packages


# ------------------------------------------------------------------------------
#   Setup
# ------------------------------------------------------------------------------

setup(
    name='Robusta',
    version='0.1.0',
    author='PacMiam',
    author_email='pacmiam@tuxfamily.org',
    description='Quick and simple web issues manager',
    long_description='Robusta is an issues manager using django framework.',
    keywords='issues tracker',
    url='https://framagit.org/kawateam/robusta',
    project_urls={
        'Source': 'https://framagit.org/kawateam/robusta',
        'Tracker': 'https://framagit.org/kawateam/robusta/issues',
    },
    packages=find_packages(exclude=['test']),
    include_package_data=True,
    python_requires='~= 3.6',
    install_requires=[
        'Django ~= 2.2',
    ],
    extras_require={
        'dev': [
            'pylint',
            'pytest',
            'flake8',
        ],
    },
    entry_points={
        'console_scripts': [
            'robusta = robusta.__main__:main',
        ],
    },
    license='AGPLv3',
    classifiers=[
        'Development Status :: 1 - Planning',
        'Environment :: Web Environment',
        'Framework :: Django :: 2.2',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: '
            'GNU Affero General Public License v3 or later (AGPLv3+)',
        'Natural Language :: French',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3 :: Only',
        'Topic :: Internet :: WWW/HTTP :: Browsers',
        'Topic :: Software Development :: Bug Tracking',
    ],
)
