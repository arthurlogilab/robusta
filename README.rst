Robusta
=======
Robusta is a lightweight issues manager writing in Python 3 and Django 2.

Authors
-------
- Aurélien Lubert, aka PacMiam <pacmiam@tuxfamily.org>

Dependencies
------------
- Python 3.6
- Django 2

Installation
------------

Packages
~~~~~~~~
To properly install Robusta on your system, the best way is to use pip and
virtualenv. If this applications are not available on your system, try to
install them from your packages manager.

They are often called 'python3-pip' and 'python3-virtualenv'.

Environment
~~~~~~~~~~~

Generate a virtual environment for Robusta:

::

    python3 -m virtualenv robusta

Activate virtualenv:

::

    source robusta/bin/activate

Install Robusta from setup.py:

::

    pip install -e .
    # Or for developpement environnement
    pip install -e .[dev]

To launch Robusta:

::

    robusta --config robusta.conf runserver

