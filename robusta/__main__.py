#!/usr/bin/env python3
# ------------------------------------------------------------------------------
#   This program is free software: you can redistribute it and/or modify it
#   under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#   License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Configuration
from configparser import ConfigParser

# Crypto
from secrets import token_hex

# Filesystem
from pathlib import Path

# System
from os import environ
from argparse import ArgumentParser


# ------------------------------------------------------------------------------
#   Functions
# ------------------------------------------------------------------------------

def generate_environment(configuration):
    """ Generate a new environment from Robusta configuration file

    Parameters
    ----------
    configuration : configparser.ConfigParser
        Configuration file parser instance
    """

    # Set Django environment variables
    environ.setdefault('DJANGO_SETTINGS_MODULE', 'robusta.settings')

    # Set Robusta environment variables
    variables = {
        # Engine variables
        'ROBUSTA_ENGINE_DEBUG':
            ('engine', 'debug', False),
        'ROBUSTA_ENGINE_HOSTNAME':
            ('engine', 'hostname', ''),
        'ROBUSTA_ENGINE_LANG':
            ('engine', 'language', 'en'),
        'ROBUSTA_ENGINE_TIMEZONE':
            ('engine', 'timezone', 'utc'),
        # Metadata variables
        'ROBUSTA_METADATA_TITLE':
            ('metadata', 'title', 'Robusta'),
        'ROBUSTA_METADATA_DESCRIPTION':
            ('metadata', 'description', ''),
        'ROBUSTA_METADATA_KEYWORDS':
            ('metadata', 'keywords', ''),
        'ROBUSTA_METADATA_AUTHORS':
            ('metadata', 'authors', ''),
        # Path variables
        'ROBUSTA_PATH_DATA':
            ('path', 'data', ''),
        'ROBUSTA_PATH_STATIC':
            ('path', 'static', ''),
        'ROBUSTA_PATH_UPLOADS':
            ('path', 'uploads', ''),
        # Custom variables
        'ROBUSTA_OPTION_UPLOAD_MAXIMUM_SIZE':
            ('option', 'upload-maximum-size', 20),
    }

    for key, (section, option, default) in variables.items():

        if isinstance(default, int):
            value = configuration.getint(section, option, fallback=default)
        elif isinstance(default, float):
            value = configuration.getfloat(section, option, fallback=default)
        elif isinstance(default, bool):
            value = configuration.getboolean(section, option, fallback=default)
        else:
            value = configuration.get(section, option, fallback=default)

        environ.setdefault(key, str(value))


def make_storage_directories():
    """ Create storage directories based on configuration values
    """

    default_directory = Path(__file__).resolve().parent.joinpath('storage')

    directories = {
        'ROBUSTA_PATH_DATA': 'data',
        'ROBUSTA_PATH_STATIC': 'static',
        'ROBUSTA_PATH_UPLOADS': 'uploads',
    }

    for key, directory in directories.items():

        path = default_directory.joinpath(directory)
        if environ[key]:
            path = Path(environ[key]).expanduser().resolve()

        if not path.exists():
            path.mkdir(parents=True)

        # Update environment variables
        environ[key] = str(path)


def make_secret_key():
    """ Create a new secret key for database purpose
    """

    secret_key = Path(environ.get('ROBUSTA_PATH_DATA')).joinpath('.id_key')

    if not secret_key.exists():

        with secret_key.open(mode='w') as pipe:
            pipe.write(token_hex())


# ------------------------------------------------------------------------------
#   Launcher
# ------------------------------------------------------------------------------

def main():
    """ Robusta launcher
    """

    parser = ArgumentParser(epilog='Copyleft Kawa-Team',
                            description='Robusta - 0.1.0',
                            conflict_handler='resolve')

    parser.add_argument('-v',
                        '--version',
                        action='version',
                        version='Robusta 0.1.0 - License AGPLv3',
                        help='show the current version')
    parser.add_argument('--config',
                        action='store',
                        metavar='FILE',
                        required=True,
                        help='set configuration file')
    parser.add_argument('actions',
                        nargs='+',
                        action='store',
                        metavar='ACTION',
                        help='django action')

    arguments = parser.parse_args()

    # ----------------------------------------
    #   Initialize configuration
    # ----------------------------------------

    robusta_conf = Path(arguments.config).resolve()

    if not robusta_conf.exists():
        raise OSError(2, "Cannot found robusta.conf in root directory")

    # Retrieve configuration file
    configuration = ConfigParser()
    configuration.read(robusta_conf)

    # ----------------------------------------
    #   Initialize engine
    # ----------------------------------------

    # Generate environment variables from configuration
    generate_environment(configuration)
    # Generate storage directories
    make_storage_directories()
    # Generate secret key
    make_secret_key()

    # Start Django engine
    try:
        from django.core.management import execute_from_command_line

        arguments.actions.insert(0, __file__)
        execute_from_command_line(arguments.actions)

    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc


if __name__ == '__main__':
    main()
