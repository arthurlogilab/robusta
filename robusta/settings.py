# ------------------------------------------------------------------------------
#   This program is free software: you can redistribute it and/or modify it
#   under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#   License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------------

# Filesystem
from pathlib import Path

# System
from os import environ


# ------------------------------------------------------------------------------
#   Paths
# ------------------------------------------------------------------------------

LOGIN_URL = '/auth/sign_in/'
MEDIA_URL = '/uploads/'
STATIC_URL = '/static/'

BASE_DIR = Path(__file__).resolve().parent.parent
LOCALE_DIR = str(BASE_DIR.joinpath('robusta', 'locale'))
STATIC_DIR = str(BASE_DIR.joinpath('robusta', 'static'))

STATIC_ROOT = environ.get('ROBUSTA_PATH_STATIC')
MEDIA_ROOT = environ.get('ROBUSTA_PATH_UPLOADS')

LOCALE_PATHS = [LOCALE_DIR]
STATICFILES_DIRS = [STATIC_DIR]

# ------------------------------------------------------------------------------
#   Secret
# ------------------------------------------------------------------------------

secret_key = Path(environ.get('ROBUSTA_PATH_DATA')).joinpath('.id_key')

if not secret_key.exists():
    raise OSError(2, "Cannot found .id_key secret file")

with secret_key.open(mode='r') as pipe:
    SECRET_KEY = ''.join(pipe.readlines())

# ------------------------------------------------------------------------------
#   Settings
# ------------------------------------------------------------------------------

# Allowed hosts on website
ALLOWED_HOSTS = ['localhost', '127.0.0.1', '[::1]']
for hostname in environ.get('ROBUSTA_ENGINE_HOSTNAME').split():
    ALLOWED_HOSTS.append(hostname)

# Language
LANGUAGE_CODE = environ.get('ROBUSTA_ENGINE_LANG')
USE_I18N = True
USE_L10N = True

# Time zone
TIME_ZONE = environ.get('ROBUSTA_ENGINE_TIMEZONE')
USE_TZ = True

# Error output options
DEBUG = bool(environ.get('ROBUSTA_ENGINE_DEBUG'))
# Session options
SESSION_EXPIRE_AT_BROWSER_CLOSE = False

# ------------------------------------------------------------------------------
#   Applications
# ------------------------------------------------------------------------------

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'robusta.urls'
WSGI_APPLICATION = 'robusta.wsgi.application'

# ------------------------------------------------------------------------------
#   Templates
# ------------------------------------------------------------------------------

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

# ------------------------------------------------------------------------------
#   Database
# ------------------------------------------------------------------------------

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': str(Path(environ['ROBUSTA_PATH_DATA']).joinpath('db.sqlite3')),
    }
}

# ------------------------------------------------------------------------------
#   Password
# ------------------------------------------------------------------------------

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.'
                'UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.'
                'MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.'
                'CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.'
                'NumericPasswordValidator',
    }
]
